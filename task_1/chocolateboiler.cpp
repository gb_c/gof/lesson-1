#include "chocolateboiler.h"


ChocolateBoiler* ChocolateBoiler::_chocolateBoiler = nullptr;
std::mutex mutex_instance;


ChocolateBoiler::ChocolateBoiler()
    : _empty(true)
    , _boiled(false)
{}


ChocolateBoiler* ChocolateBoiler::getInstance()
{
    std::lock_guard<std::mutex> lock(mutex_instance);

    if(_chocolateBoiler == nullptr)
    {
        _chocolateBoiler = new ChocolateBoiler();
    }

    return _chocolateBoiler;
}


bool ChocolateBoiler::isEmpty() const
{
    return _empty;
}


bool ChocolateBoiler::isBoiled() const
{
    return _boiled;
}


void ChocolateBoiler::fill()
{
    if(isEmpty()){
        _empty = false;
        _boiled = false;
    }
}


void ChocolateBoiler::drain()
{
    if(!isEmpty() && isBoiled())
    {
        _empty = true;
    }
}


void ChocolateBoiler::boil()
{
    if(!isEmpty() && !isBoiled())
    {
        _boiled = true;
    }
}
