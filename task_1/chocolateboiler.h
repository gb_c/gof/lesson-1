#ifndef CHOCOLATEBOILER_H
#define CHOCOLATEBOILER_H

#include <iostream>
#include <mutex>


class ChocolateBoiler
{
private:
    bool _empty;
    bool _boiled;

    ChocolateBoiler();
    static ChocolateBoiler* _chocolateBoiler;

public:
    static ChocolateBoiler* getInstance();
    ChocolateBoiler(ChocolateBoiler& other) = delete;
    void operator=(ChocolateBoiler&) = delete;

    bool isEmpty() const;
    bool isBoiled() const;
    void fill();
    void drain();
    void boil();
};

#endif // CHOCOLATEBOILER_H
