#include <iostream>
#include <vector>
#include "chocolateboiler.h"


int main()
{
    ChocolateBoiler *cb1 = ChocolateBoiler::getInstance();
    std::cout << std::boolalpha << "cb1 is empty = " << cb1->isEmpty() << std::endl;
    cb1->fill();
    std::cout << std::boolalpha << "cb1 is empty = " << cb1->isEmpty() << std::endl;

    ChocolateBoiler *cb2 = ChocolateBoiler::getInstance();
    std::cout << std::boolalpha << "cb2 is empty = " << cb2->isEmpty() << std::endl;

    return 0;
}
