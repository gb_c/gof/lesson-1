cmake_minimum_required(VERSION 3.14)
project(task_2)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS -pthread)

set(SOURCE_FILES
    main.cpp
    automaton.cpp
    automaton.h
    proxyautomaton.cpp
    proxyautomaton.h
)

add_executable(${PROJECT_NAME} ${SOURCE_FILES})
