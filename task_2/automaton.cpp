#include "automaton.h"


Automaton::Automaton(const std::string &location, double recievedMoney)
    : location(location)
    , recieved_money(recievedMoney)
{
    spent_money = 0.0;
}


void Automaton::changePrice(std::string name, float newPrice)
{
    goods_price[name] = newPrice;
}


void Automaton::sellProduct(std::string name)
{
    if(goods_count[name] > 0)
    {
        recieved_money += goods_price[name];
    }
    else
    {
        std::cerr << "Not enough goods" << std::endl;
    }
}


void Automaton::addProduct(std::string name, size_t count)
{
    goods_count[name] += count;
}


double Automaton::allMoneyReport() const
{
    return recieved_money - spent_money;
}


const std::string& Automaton::getLocationReport() const
{
    return location;
}


std::map<std::string, size_t> Automaton::goodsCountReport() const
{
    return goods_count;
}


std::map<std::string, float> Automaton::goodsPriceReport() const
{
    return goods_price;
}
