#ifndef AUTOMATON_H
#define AUTOMATON_H

#include <iostream>
#include <map>


class ISellSystem
{
public:
    virtual void changePrice(std::string, float) = 0;
    virtual void sellProduct(std::string) = 0;
    virtual void addProduct(std::string, size_t) = 0;
};


class Automaton: public ISellSystem
{
private:
    std::string location;
    double recieved_money;
    double spent_money;
    std::map<std::string, float> goods_price;
    std::map<std::string, size_t> goods_count;

public:
    Automaton(const std::string &location, double recievedMoney);

    void changePrice(std::string name, float newPrice) override;
    void sellProduct(std::string name) override;
    void addProduct(std::string name, size_t count) override;
    double allMoneyReport() const;
    const std::string& getLocationReport() const;
    std::map<std::string, size_t> goodsCountReport() const;
    std::map<std::string, float> goodsPriceReport() const;
};

#endif // AUTOMATON_H
