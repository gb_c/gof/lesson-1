#include <iostream>
#include <vector>
//#include <thread>
//#include <chrono>
#include "proxyautomaton.h"


int main()
{
    Automaton *a1 = new Automaton("floor 1", 200.00);
    a1->addProduct("Toy1", 3);
    a1->addProduct("Toy2", 7);
    a1->changePrice("Toy1", 40.0);
    a1->changePrice("Toy2", 25.0);
    a1->sellProduct("Toy2");
    std::cout << "All money: " << a1->allMoneyReport() << std::endl;
    std::cout << "Location: " << a1->getLocationReport() << std::endl;

    std::cout << "Goods count:" << std::endl;

    for(auto thing: a1->goodsCountReport())
    {
        std::cout << thing.first << " : " << thing.second << std::endl;
    }

    std::cout << "Goods price:" << std::endl;

    for(auto thing: a1->goodsPriceReport())
    {
        std::cout << thing.first << " : " << thing.second << std::endl;
    }

    std::cout << "All money: " << a1->allMoneyReport() << std::endl;
    std::cout << "Location: " << a1->getLocationReport() << std::endl << std::endl;


    ProxyAutomaton *pa1 = new ProxyAutomaton("floor 2", 100.00);
    pa1->addProduct("Toy1", 3);
    pa1->addProduct("Toy2", 7);
    pa1->changePrice("Toy1", 40.0);
    pa1->changePrice("Toy2", 25.0);
    pa1->sellProduct("Toy2");
    std::cout << "All money: " << pa1->allMoneyReport() << std::endl;
    std::cout << "Location: " << pa1->getLocationReport() << std::endl;

//    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::cout << "Goods count:" << std::endl;

    for(auto thing: pa1->goodsCountReport())
    {
        std::cout << thing.first << " : " << thing.second << std::endl;
    }

    std::cout << "Goods price:" << std::endl;

    for(auto thing: pa1->goodsPriceReport())
    {
        std::cout << thing.first << " : " << thing.second << std::endl;
    }

    std::cout << "All money: " << pa1->allMoneyReport() << std::endl;
    std::cout << "Location: " << pa1->getLocationReport() << std::endl;

    return 0;
}
