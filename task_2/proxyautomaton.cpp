#include "proxyautomaton.h"


ProxyAutomaton::ProxyAutomaton(const std::string &location, double recievedMoney)
    : _automation(new Automaton(location, recievedMoney))
{}


void ProxyAutomaton::changePrice(std::string name, float newPrice)
{
    _automation->changePrice(name, newPrice);
}


void ProxyAutomaton::sellProduct(std::string name)
{
    _automation->sellProduct(name);
}


void ProxyAutomaton::addProduct(std::string name, size_t count)
{
    _automation->addProduct(name, count);
}


double ProxyAutomaton::allMoneyReport() const
{
    updateData();

    return _allMoney;
}


const std::string& ProxyAutomaton::getLocationReport() const
{
    updateData();

    return _location;
}


std::map<std::string, size_t> ProxyAutomaton::goodsCountReport() const
{
    updateData();

    return _goodsCount;
}


std::map<std::string, float> ProxyAutomaton::goodsPriceReport() const
{
    updateData();

    return _goodsPrice;
}


bool ProxyAutomaton::checkTime() const
{
    std::time_t now = std::time(0);
    return (now - _lastReport > FREEZE_REPORT);
}


void ProxyAutomaton::updateData() const
{
    if(checkTime())
    {
        _allMoney = _automation->allMoneyReport();
        _location = _automation->getLocationReport();
        _goodsCount = _automation->goodsCountReport();
        _goodsPrice = _automation->goodsPriceReport();
        _lastReport = std::time(0);

        std::cout << "(get data) ";
    }
    else
    {
        std::cout << "(from memory) ";
    }
}
