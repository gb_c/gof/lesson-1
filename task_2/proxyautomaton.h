#ifndef PROXYAUTOMATON_H
#define PROXYAUTOMATON_H

//#define FREEZE_REPORT 1
#define FREEZE_REPORT 3600

#include <ctime>
#include "automaton.h"


class ProxyAutomaton : public ISellSystem
{
private:
    Automaton* _automation;

    mutable std::time_t _lastReport = 0;
    mutable double _allMoney;
    mutable std::string _location;
    mutable std::map<std::string, float> _goodsPrice;
    mutable std::map<std::string, size_t> _goodsCount;

    bool checkTime() const;
    void updateData() const;

public:
    ProxyAutomaton(const std::string &location, double recievedMoney);

    void changePrice(std::string name, float newPrice) override;
    void sellProduct(std::string name) override;
    void addProduct(std::string name, size_t count) override;
    double allMoneyReport() const;
    const std::string& getLocationReport() const;
    std::map<std::string, size_t> goodsCountReport() const;
    std::map<std::string, float> goodsPriceReport() const;
};

#endif // PROXYAUTOMATON_H
